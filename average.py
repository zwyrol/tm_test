import app
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--type', help='The service type. example: UserService', required=True)


args = parser.parse_args()

services = app.service.get_all_by_type(args.type)
calculator = app.service.ServiceAverageCalculator(services)

app.service.display(services)

print("Results for: {}".format(args.type))
print("CPU Average: {}%".format(calculator.cpu_average()))
print("Memory Average: {}%".format(calculator.memory_average()))
