from app.config import API_URL
import unittest
import requests


class TestAPI(unittest.TestCase):
    def test_api_is_working(self):
        response = requests.get("{}/servers".format(API_URL))
        self.assertTrue(response.ok)

