from app.config import API_URL, RESOURCE_CRITICAL_LEVEL, HEALTHY_INSTANCES_RUNNING_LIMIT
import unittest
import requests
from unittest.mock import patch
from app.service import (
    get_all,
    get_all_by_type,
    get_service,
    get_services_ip_addresses,
    Service,
    ServiceAverageCalculator,
    ServiceHealthChecker
)
from app.service.classess import ServiceListEmptyException

import random


class TestServerModule(unittest.TestCase):
    @patch('app.api.requests.get')
    def test_getting_all_services_ips(self, mock_get):
        servers_list = b'["10.58.1.1", "10.58.1.2"]'
        mock_get.return_value.content = servers_list

        servers = get_services_ip_addresses()

        self.assertEqual(servers[0], '10.58.1.1')
        self.assertEqual(servers[1], '10.58.1.2')

    @patch('app.api.requests.get')
    def test_getting_service_details(self, mock_get):
        mock_get.return_value.content = b'{"cpu": "51%", "memory": "95%", "service": "MLService"}'

        ip = '10.58.1.1'
        instance = get_service(ip)
        self.assertEqual(type(instance.cpu), float)
        self.assertEqual(instance.cpu, 51.0)
        self.assertEqual(type(instance.memory), float)
        self.assertEqual(instance.memory, 95)
        self.assertEqual(type(instance.name), str)
        self.assertEqual(instance.name, 'MLService')
        self.assertEqual(type(instance.ip), str)
        self.assertEqual(instance.ip, ip)

    def test_service_instance_invalid_data(self):
        details = {
            'ip': '10.0.0.1',
            'cpu': '33%',
            'memory': 22,
            'service': 'FooService'
        }

        with self.assertRaises(ValueError):
            Service(**details)

        details = {
            'ip': '10.0.0.1',
            'cpu': 30,
            'memory': '50%',
            'service': 'FooService'
        }

        with self.assertRaises(ValueError):
            Service(**details)

        details = {
            'ip': '',
            'cpu': '33%',
            'memory': '22%',
            'service': 'FooService'
        }

        with self.assertRaises(ValueError):
            Service(**details)

        details = {
            'ip': '10.58.1.1',
            'memory': '22%',
            'service': 'FooService'
        }

        with self.assertRaises(ValueError):
            Service(**details)

        details = {
            'ip': '10.58.1.1',
            'cpu': '33%',
            'service': 'FooService'
        }

        with self.assertRaises(ValueError):
            Service(**details)

        details = {
            'ip': '10.58.1.1',
            'cpu': '33%',
            'memory': '23',
            'service': ''
        }

        with self.assertRaises(ValueError):
            Service(**details)

        details = {
            'ip': '10.58.1.1',
            'cpu': 'string',
            'memory': '23',
            'service': 'Testservice'
        }

        with self.assertRaises(ValueError):
            Service(**details)

        details = {
            'ip': '10.58.1.1',
            'cpu': '25%',
            'memory': 'string',
            'service': 'Testservice'
        }

        with self.assertRaises(ValueError):
            Service(**details)

        details = {
            'cpu': '25%',
            'memory': '50%',
            'service': 'Testservice'
        }

        with self.assertRaises(ValueError):
            Service(**details)

        details = {
            'ip': '10.58.1.1',
            'cpu': '25%',
            'memory': '66%',
        }

        with self.assertRaises(ValueError):
            Service(**details)

    def test_service_details_instance(self):
        details = {
            'ip': '10.58.1.1',
            'cpu': '33%',
            'memory': '22%',
            'service': 'FooService'
        }

        instance = Service(**details)

        self.assertEqual(type(instance.cpu), float)
        self.assertEqual(type(instance.memory), float)
        self.assertEqual(type(instance.name), str)
        self.assertEqual(instance.name, details['service'])
        self.assertEqual(instance.cpu, 33.0)
        self.assertEqual(instance.memory, 22.0)

    def test_service_is_healthy(self):
        service = Service(ip='10.58.1.1', service='FooService', cpu='45%', memory='50%')

        self.assertTrue(service.is_healthy())

        service.cpu = RESOURCE_CRITICAL_LEVEL

        self.assertFalse(service.is_healthy())

        service.cpu = 50
        service.memory = RESOURCE_CRITICAL_LEVEL

        self.assertFalse(service.is_healthy())

        service.cpu = RESOURCE_CRITICAL_LEVEL + 10
        service.memory = RESOURCE_CRITICAL_LEVEL + 10

        self.assertFalse(service.is_healthy())

        service.cpu = RESOURCE_CRITICAL_LEVEL - 10
        service.memory = RESOURCE_CRITICAL_LEVEL - 10

        self.assertTrue(service.is_healthy())

    def test_average_calculator_wrong_data(self):
        with self.assertRaises(ServiceListEmptyException):
            ServiceAverageCalculator([])

    def test_service_average_calculator(self):
        data = [
            Service(ip='10.58.1.1', service='FooService', cpu='30%', memory='50%'),
            Service(ip='10.58.1.2', service='FooService', cpu='60%', memory='90%')
        ]

        calculator = ServiceAverageCalculator(data)
        cpu_average = calculator.cpu_average()
        memory_average = calculator.memory_average()

        self.assertEqual(type(cpu_average), float)
        self.assertEqual(type(memory_average), float)
        self.assertEqual(calculator.cpu_values(), [30, 60])
        self.assertEqual(cpu_average, 45.0)
        self.assertEqual(memory_average, 70)
        self.assertEqual(calculator.memory_values(), [50, 90])

    @patch('app.service.handlers.get_all')
    def test_getting_services_by_type(self, mock):
        data = [
            Service(ip='10.58.1.1', service='FooService', cpu='30%', memory='50%'),
            Service(ip='10.58.1.2', service='FooService', cpu='60%', memory='90%'),
            Service(ip='10.58.1.3', service='AnotherService', cpu='60%', memory='90%'),
        ]
        mock.return_value = data

        services = get_all_by_type('FooService')

        self.assertEqual(len(services), 2)

        for service in services:
            self.assertIsInstance(service, Service)
            self.assertEqual(service.name, 'FooService')

    @patch('app.service.handlers.get_all')
    def test_service_healthy_checker(self, mock):
        data = []

        for i in range(HEALTHY_INSTANCES_RUNNING_LIMIT + 5):
            data.append(Service(ip='10.58.2.{}'.format(i),
                                service='AnotherService',
                                cpu='{}%'.format(random.randrange(1, RESOURCE_CRITICAL_LEVEL-1)),
                                memory='{}%'.format(random.randrange(1, RESOURCE_CRITICAL_LEVEL-1))
                                ))

        for i in range(HEALTHY_INSTANCES_RUNNING_LIMIT):
            data.append(Service(ip='10.58.1.{}'.format(i),
                                service='FooService',
                                cpu='{}%'.format(RESOURCE_CRITICAL_LEVEL+5),
                                memory='{}%'.format(RESOURCE_CRITICAL_LEVEL+5),
                                ))

        mock.return_value = data

        health_checker = ServiceHealthChecker(data)
        self.assertEqual(health_checker.get_critical_health_instances(), ['FooService'])

    def test_service_healthy_checker_no_services(self):
        with self.assertRaises(ServiceListEmptyException):
            ServiceHealthChecker([])
