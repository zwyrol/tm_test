I've tested following application in python 3.5 and python 3.7.

Install:
git clone git@bitbucket.org:zwyrol/tm_test.git
virtualenv --python=`which python3` .
source bin/activate
pip install -r requirements.txt


Commands:

python show.py -h
python average.py -h
python unhealthy.py

When running average.py and unhealthy.py I displaying also which services are involved on top.

Config:

API_URL- The API URL
RESOURCE_CRITICAL_LEVEL - If CPU/Memory reach this level it will class service as unhealthy
HEALTHY_INSTANCES_RUNNING_LIMIT - It's used for the flag services which have fewer healthy instances running than this limit. 



Running Unit Test 

This test checking only if API is working. Rest of tests are not depends of API

python3 -m unittest tests/api_test.py 

Service test - The main unit test with most crucial elements of application. I using mocking to simulate API calls.

python3 -m unittest tests/service_test.py


What I can improve ?

1. Implement error handling of errors/exceptions when I getting services/services ips or service by type. I just assumed that API will work all the time and even if does not I would see exception about wrong connection when I run the script.
2. Write documentation to functions
3. Display all available types of services we support in help
5. I could improve commands themselves to be classes and have one common parent or build that as pip package and setup commands in entry points.
6. I also could improve a test which testing invalid data when we creating Service instance to use loop than hard coded data.
7. For show.py I think running with system 'watch' command every 2 secs would be more appreciate than writing and running that in infinite loop by python script
