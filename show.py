import app
import argparse
import os
from time import sleep


parser = argparse.ArgumentParser()
parser.add_argument('--type', help='The service type. example: UserService')
parser.add_argument('--forever', help='Running in infinite loop', action='store_true')

args = parser.parse_args()

while True:
    if args.type:
        services = app.service.get_all_by_type(args.type)
    else:
        services = app.service.get_all()

    os.system('clear')
    app.service.display(services)

    if args.forever:
        sleep(2)
    else:
        quit()
