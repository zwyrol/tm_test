from app.config import RESOURCE_CRITICAL_LEVEL, HEALTHY_INSTANCES_RUNNING_LIMIT


class ServiceListEmptyException(Exception):
    pass


class ServiceHealthChecker:
    def __init__(self, services):
        self.services = services

        if len(self.services) == 0:
            raise ServiceListEmptyException(
                'The services list is empty and I can\'t check instances health at the moment'
            )

    def get_critical_health_instances(self):
        data = {}
        for service in self.services:
            if service.name not in data:
                data[service.name] = 0

            if service.is_healthy():
                data[service.name] += 1

        return [x for x in data if data[x] < HEALTHY_INSTANCES_RUNNING_LIMIT]


class ServiceAverageCalculator:
    def __init__(self, services):
        self.services = services

        if len(self.services) == 0:
            raise ServiceListEmptyException('The services list is empty and I can\'t count average at the moment')

    def cpu_values(self):
        return [service.cpu for service in self.services]

    def memory_values(self):
        return [service.memory for service in self.services]

    def cpu_average(self):
        return round(sum(self.cpu_values()) / len(self.services), 2)

    def memory_average(self):
        return round(sum(self.memory_values()) / len(self.services), 2)


class Service:
    def __init__(self, **kwargs):
        self.ip = kwargs.get('ip', False)
        self.name = kwargs.get('service', False)

        try:
            self.cpu = float(kwargs['cpu'].strip('%'))
        except (ValueError, KeyError, AttributeError):
            raise ValueError('Running Instance CPU is invalid')

        try:
            self.memory = float(kwargs['memory'].strip('%'))
        except (ValueError, KeyError, AttributeError):
            raise ValueError('Running Instance Memory is invalid')

        if not self.ip:
            raise ValueError('Running Instance ip is not provided')

        if not self.name:
            raise ValueError('Running Instance name is not provided')

    def is_healthy(self):
        if self.cpu >= RESOURCE_CRITICAL_LEVEL or self.memory >= RESOURCE_CRITICAL_LEVEL:
            return False

        return True

    def __str__(self):
        return "{} - {}: CPU: {}%, Memory: {}%, Is Healthy: {}".format(self.ip,
                                                                       self.name,
                                                                       self.cpu,
                                                                       self.memory,
                                                                       self.is_healthy())

