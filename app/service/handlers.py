from app.api import ApiClient
from app.config import API_URL
from app.service import Service
from prettytable import PrettyTable


def get_services_ip_addresses():
    return ApiClient.get('{}/servers'.format(API_URL))


def get_service(ip):
    details = ApiClient.get('{}/{}'.format(API_URL, ip))
    return Service(ip=ip, **details)


def get_all():
    services = []

    for ip in get_services_ip_addresses():
        services.append(get_service(ip))

    return services


def get_all_by_type(service_type):
    services = get_all()

    return [service for service in services if service.name == service_type]


def display(services):
    table = PrettyTable()

    table.field_names = ['IP', 'Service', 'Status', 'CPU', 'Memory']

    for service in services:
        table.add_row([
            service.ip,
            service.name,
            'Healthy' if service.is_healthy() else 'Unhealthy',
            '{}%'.format(service.cpu),
            '{}%'.format(service.memory)
        ])

    print(table)
