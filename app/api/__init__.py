import requests
import json


class ApiClient:
    @staticmethod
    def get(url):
        response = requests.get(url)
        content = response.content.decode()
        return json.loads(content)
