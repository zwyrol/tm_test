import app

services = app.service.get_all()

app.service.display(services)
unhealthy_instances = app.service.ServiceHealthChecker(services).get_critical_health_instances()

if len(unhealthy_instances) > 0:
    print('Instances which fewer than {} healthy instances: {}'.format(
        app.config.HEALTHY_INSTANCES_RUNNING_LIMIT,
        ', '.join(unhealthy_instances)
    ))
else:
    print('No instances which fewer than {} healthy instances'.format(app.config.HEALTHY_INSTANCES_RUNNING_LIMIT))
